from math import sqrt
from hpp import Transform
from hpp.corbaserver.manipulation import ConstraintGraph,Constraints
from hpp.corbaserver import Client
Client ().problem.resetProblem ()
from manipulation import robot, vf, ps, Ground, Box, Pokeball, PathPlayer, gripperName, ballName
import sys

vf.loadEnvironmentModel (Ground, 'ground')
vf.loadEnvironmentModel (Box, 'box')
vf.moveObstacle ('box/base_link_0', [0.3+0.04, 0, 0.04, 0, 0, 0, 1])
vf.moveObstacle ('box/base_link_1', [0.3-0.04, 0, 0.04, 0, 0, 0, 1])
vf.moveObstacle ('box/base_link_2', [0.3, 0.04, 0.04, 0, 0, 0, 1])
vf.moveObstacle ('box/base_link_3', [0.3, -0.04, 0.04, 0, 0, 0, 1])

vf.loadObjectModel (Pokeball, 'pokeball')
robot.setJointBounds ('pokeball/root_joint', [-.4,.4,-.4,.4,-.1,1.,
                                              -1.0001, 1.0001,-1.0001, 1.0001,
                                              -1.0001, 1.0001,-1.0001, 1.0001,])

q1 = [0, -1.57, 1.57, 0, 0, 0, .3, 0, 0.025, 0, 0, 0, 1]

## Create constraint graph
graph = ConstraintGraph (robot, 'graph')

## Create constraint of relative position of the ball in the gripper when ball
## is grasped
ballInGripper = [0, .137, 0, 0.5, 0.5, -0.5, 0.5]
ballUnderGripper = [0, .237, 0, 0.5, 0.5, -0.5, 0.5]

## Create nodes and edges
#  Warning the order of the nodes is important. When checking in which node
#  a configuration lies, node constraints will be checked in the order of node
#  creation.
graph.createNode (['gripper-above-ball','ball-above-ground','ball-above-objective','grasp-objective','grasp-placement','placement','grasp'])

graph.createEdge ('placement', 'placement'                ,'transit', 1          ,'placement')
graph.createEdge ('placement','gripper-above-ball'        ,'approach-ball',1     ,'placement')
graph.createEdge ('gripper-above-ball','placement'        ,'move-gripper-away',1 ,'placement')
graph.createEdge ('gripper-above-ball','grasp-placement'  ,'grasp-ball',1        ,'placement')
graph.createEdge ('grasp-objective','gripper-above-ball'  ,'move-gripper-up',1   ,'placement')
graph.createEdge ('grasp-placement','ball-above-ground'   ,'take-ball-up',1      ,'grasp')
graph.createEdge ('ball-above-objective','grasp-objective','put-ball-down',1     ,'grasp')
graph.createEdge ('ball-above-ground','ball-above-objective' ,'take-ball-away',1    ,'grasp')


ps.createTransformationConstraint ('grasp', gripperName, ballName,
                                   ballInGripper, 6*[True,])
ps.createTransformationConstraint('placementAboveBall',gripperName,ballName,
                                   ballUnderGripper,6*[True,])

## Create transformation constraint : ball is in horizontal plane with free
## rotation around z
ps.createTransformationConstraint ('placementBallOnGround', '', ballName,
                                   [0,0,0.025,0, 0, 0, 1],
                                   [False, False, True, True, True, False,])
ps.createTransformationConstraint ('placementBallAboveGround', '', ballName,
                                   [0.3,0,0.120,0, 0, 0, 1],
                                   [True, True, True,  True,True,False,])
ps.createTransformationConstraint ('placementBallAboveObjective', '', ballName,
                                   [.3, 0.2, 0.120, 0, 0, 0, 1],
                                   [True, True, True,  True, True, False,])
ps.createTransformationConstraint ('placementBallOnObjective', '', ballName,
                                   [.3, .2, 0.025,0, 0, 0, 1],
                                   [True, True, True,  True,True,True,])


#  Create complement constraint
ps.createTransformationConstraint ('placementBallOnGround/complement', '', ballName,
                                   [0,0,0.025,0, 0, 0, 1],
                                   [True,True, False, False, False, True,])


ps.setConstantRightHandSide ('placementBallOnGround', True)
ps.setConstantRightHandSide ('placementBallAboveGround', True)

ps.setConstantRightHandSide ('placementBallAboveObjective', True)
ps.setConstantRightHandSide ('placementBallOnGround/complement', False)
ps.setConstantRightHandSide ('placementAboveBall',True)

## Set constraints of nodes and edges
graph.addConstraints (node='placement'           , constraints = Constraints (numConstraints = ['placementBallOnGround'],))
graph.addConstraints (node='gripper-above-ball'  , constraints = Constraints (numConstraints = ['placementBallOnGround','placementAboveBall'],))
graph.addConstraints (node='grasp-placement'     , constraints = Constraints (numConstraints = ['grasp','placementBallOnGround']))
graph.addConstraints (node='grasp-objective'     , constraints = Constraints (numConstraints = ['grasp','placementBallOnObjective']))
graph.addConstraints (node='ball-above-ground'   , constraints = Constraints (numConstraints = ['grasp','placementBallAboveGround']))
graph.addConstraints (node='ball-above-objective', constraints = Constraints (numConstraints = ['grasp','placementBallAboveObjective']))
graph.addConstraints (node='grasp'               , constraints = Constraints (numConstraints = ['grasp']))

graph.addConstraints (edge='transit'          , constraints = Constraints (numConstraints = ['placementBallOnGround/complement']))
graph.addConstraints (edge='approach-ball'    , constraints = Constraints (numConstraints = ['placementBallOnGround/complement']))
graph.addConstraints (edge='move-gripper-away', constraints = Constraints (numConstraints = ['placementBallOnGround/complement'])) 
graph.addConstraints (edge='move-gripper-up'  , constraints = Constraints (numConstraints = ['placementBallOnGround/complement']))
graph.addConstraints (edge='grasp-ball'       , constraints = Constraints (numConstraints = ['placementBallOnGround/complement']))
# These edges are in state 'grasp'
graph.addConstraints (edge='take-ball-up'     , constraints = Constraints ())
graph.addConstraints (edge='put-ball-down'  ,   constraints = Constraints ())
graph.addConstraints (edge='take-ball-away'   , constraints = Constraints ())

#note : the 'grasp' node is and isolated lead but is still useful to create all the edge that grasp the ball
# I did not find a cleaner way to get everything working

ps.selectPathValidation ("Dichotomy", 0)
ps.selectPathProjector ("Progressive", 0.1)
graph.initialize ()

## Project initial configuration on state 'placement'
res, q_init, error = graph.applyNodeConstraints ('placement', q1)
q2 = q1 [::]
q2 [7] = .2

## Project goal configuration on state 'placement'
res, q_goal, error = graph.applyNodeConstraints ('placement', q2)

## Define manipulation planning problem
ps.setInitialConfig (q_init)
ps.addGoalConfig (q_goal)

v = vf.createViewer ()
pp = PathPlayer (v)
# v (q1)

def solve():
  print("Solver started")
  ps.solve()
  print("Solution found, let's display it")
  pp = PathPlayer(v)
  pp(0)

if('grasp_ball_in_box.py' in sys.argv[0]):
  print("execute solve() to resolve and display the path")

