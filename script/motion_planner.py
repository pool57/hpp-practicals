class MotionPlanner:
  def __init__ (self, robot, ps):
    self.robot = robot
    self.ps = ps
    
  def seekCloserEdgeAndAddLongestEdgeToRoadmap(self,configSelected,connectComponentIdToConsider,configs):
      [nearestConfigInRoadmap,_] = self.ps.getNearestConfig(configSelected,connectedComponentId=connectComponentIdToConsider)
      [_,path,_] = self.ps.directPath(nearestConfigInRoadmap,configSelected,True)
      pathLength = self.ps.pathLength(path)
      configBtwSelectionAndNearestConfigInRoadmap = self.ps.configAtParam(path,pathLength)
      self.ps.addConfigToRoadmap(configBtwSelectionAndNearestConfigInRoadmap)
      self.ps.addEdgeToRoadmap(nearestConfigInRoadmap,configBtwSelectionAndNearestConfigInRoadmap,path,True)
      configs.append(configBtwSelectionAndNearestConfigInRoadmap)
      
  def seekConnection(self,configsA,configB):
     for configA in configsA:
      	   [isConnected,pathTest,_] = self.ps.directPath(configA,configB,True)
      	   if isConnected:
      	   	self.ps.addEdgeToRoadmap(configA,configB,pathTest,True)
  
  

  def solveBiRRT (self, maxIter = float("inf")):
    self.ps.prepareSolveStepByStep ()
    finished = False

    # In the framework of the course, we restrict ourselves to 2 connected components.
    nbCC = self.ps.numberConnectedComponents ()
    if nbCC != 2:
      raise Exception ("There should be 2 connected components.")

    iter = 0
    
    configsStart = list()
    configsStart.append(self.ps.getInitialConfig())
    configsEnd = list()
    configsEnd.append(self.ps.getGoalConfigs()[0])
    while True:
      #### RRT begin
      
      configSelected = self.robot.shootRandomConfig()
      
      self.seekCloserEdgeAndAddLongestEdgeToRoadmap(configSelected,0,configsStart)    #add considering connected components from start
      self.seekCloserEdgeAndAddLongestEdgeToRoadmap(configSelected,1,configsEnd)    #add considering connected components from end
      
      ## Try connecting the new nodes together
      self.seekConnection(configsStart,configsEnd[-1])
      self.seekConnection(configsEnd,configsStart[-1])
      	
      #### RRT end
      ## Check if the problem is solved.
      nbCC = self.ps.numberConnectedComponents ()
      if nbCC == 1:
        # Problem solved
        finished = True
        break
      iter = iter + 1
      if iter%100 == 0:
      	print("iteration {}/{}".format(iter,maxIter))
      if iter > maxIter:
        break
    if finished:
        self.ps.finishSolveStepByStep ()
        return self.ps.numberPaths () - 1

  def solvePRM (self):
    self.ps.prepareSolveStepByStep ()
    iter = 0
    #### PRM begin 
    while True:
      configSelected = self.robot.shootRandomConfig()      
      
      ## Check if the problem is solved.
      nbCC = self.ps.numberConnectedComponents ()
      if nbCC == 1:
        # Problem solved
        finished = True
        break
      iter = iter + 1
      if iter > maxIter:
        break
    #### PRM end
    self.ps.finishSolveStepByStep ()
