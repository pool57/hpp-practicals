from grasp_ball_in_box import q_init, q_goal, robot, ps, graph,  pp,v
import sys
success = False
nTrial = 0

list_of_edges = ['approach-ball','grasp-ball','take-ball-up','take-ball-away','put-ball-down','move-gripper-up','move-gripper-away']

def generatePathOnEdge(edge,qStart):
    sys.stdout.write("research for edge {} linked to {}...".format(edge,graph.getNodesConnectedByEdge(edge)[1])) # I use stoud to use carrier return
    sys.stdout.flush()
    pid = -1
    res1 = False
    res2 = False
    MAXTEST = 3000

    for i in range (1,MAXTEST):
        sys.stdout.write("\rresearch for edge {} linked to {} essai {}/{}".format(edge,graph.getNodesConnectedByEdge(edge)[1],i,MAXTEST))
        sys.stdout.flush()
        q = robot.shootRandomConfig ()
        res1,qi,_ = graph.generateTargetConfig (edge, qStart, q)
        if res1 and robot.isConfigValid (qi):
            res2, pid, msg = ps.directPath(qStart, qi, True)
            if res2 :         # if we have a direct path to a random valid position corresponding to the target config, let's continue path-planning
                break;            
    
    sys.stdout.write("\rresearch for edge {} linked to {} {} at essai {}/{}\n".format(edge,graph.getNodesConnectedByEdge(edge)[1],"success" if (res1 and res2) else "failed",i,MAXTEST))
    return res1 and res2 ,qi,pid




while not success:
    if nTrial > 100 : break;
    paths = list ()
    print ("nTrial {}".format (nTrial)); nTrial += 1
    qStart = q_init
    for edge in list_of_edges:
        res,qi,pid = generatePathOnEdge(edge,qStart)
        if not res: break
        paths.append (pid)
        qStart = qi
        v(qi)
    if not res : break
    res,pid,_ = ps.directPath(qi,q_goal,True) # we add the final path to the q_goal position
    if res:
        paths.append(pid)
    else :
        print("error in last step")    

    success = len(paths) >= len(list_of_edges)

print("solve done, let's display the path obtained")
for p in paths:
    pp(p)

